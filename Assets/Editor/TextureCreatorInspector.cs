﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(TextureCreator))]
public class TextureCreatorInspector : Editor
{

    #region Properties

    /// <summary>
    /// Reference to the texture creator
    /// </summary>
    private TextureCreator textureCreator;

    #endregion

    #region Events

    /// <summary>
    /// Called when the object is enabled
    /// </summary>
    private void OnEnable()
    {
        textureCreator = target as TextureCreator;
        Undo.undoRedoPerformed += RefreshCreator;
    }

    /// <summary>
    /// Called when the object is disabled
    /// </summary>
    private void OnDisable()
    {
        Undo.undoRedoPerformed -= RefreshCreator;
    }

    /// <summary>
    /// Called while the inspector is visible
    /// </summary>
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();
        TextureCreator textureCreator = (TextureCreator)target;

        if (textureCreator.textureType == TextureType.Gradient)
        {
            EditorGUI.BeginChangeCheck();
            SerializedObject gradientContainer = new SerializedObject(textureCreator);
            SerializedProperty gradient = gradientContainer.FindProperty("gradient");
            EditorGUILayout.PropertyField(gradient, true, null);
            if (EditorGUI.EndChangeCheck())
            {
                gradientContainer.ApplyModifiedProperties();
            }
        }
        else if(textureCreator.textureType == TextureType.HeightMap)
        {
            EditorGUI.BeginChangeCheck();
            Color color = EditorGUILayout.ColorField(textureCreator.color);
            if (EditorGUI.EndChangeCheck())
            {
                textureCreator.color = color;
            }
        }
        if (GUILayout.Button("Generate Texture"))
        {
            textureCreator.GenerateTexture();
        }
        if (GUILayout.Button("Apply Noise to Texture"))
        {
            textureCreator.ApplyNoiseToTexture();
        }
        if (GUILayout.Button("Save Texture"))
        {
            textureCreator.SaveTexture();
        }
    }

    #endregion

    #region Private Methods

    /// <summary>
    /// Tells the texture creator to regenerate the texture
    /// </summary>
    private void RefreshCreator()
    {
        if (Application.isPlaying)
        {
            textureCreator.FillNoiseArray();
        }
    }

    /// <summary>
    /// Handles the creation of grid fields if the texture type is set to a grid type
    /// </summary>
    private void HandleGridControls(TextureCreator textureCreator)
    {

    }

#endregion

}
