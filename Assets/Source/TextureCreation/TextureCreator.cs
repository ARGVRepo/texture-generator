﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEngine;

public class TextureCreator : MonoBehaviour
{

    #region Properties

    #region Public

    /// <summary>
    /// Reference to the meshRenderer of the object
    /// </summary>
    public MeshRenderer meshRenderer;

    /// <summary>
    /// Chooses the type of noise used when generating the texture
    /// </summary>
    [Space(10)]
    public NoiseType noiseType;

    /// <summary>
    /// Set to true to use clamped values when generating the height map
    /// </summary>
    public bool terracedHeightMap;

    /// <summary>
    /// Set to true to apply blending between the terraces of the heightmap
    /// </summary>
    public bool blendHeightMapTerraces;


    public float blendFactor = 1f;

    /// <summary>
    /// Seed used when generating the perlin noise
    /// </summary>
    [Space(10)]
    public int seed = 12345;

    /// <summary>
    /// Sets the number of time the noise will be added
    /// </summary>
    [Range(1, 8)]
    public int octaves;

    /// <summary>
    /// Sets the factor by which the frequency changes each time the noise is added
    /// </summary>
    [Range(1f, 4f)]
    public float lacunarity = 2f;

    /// <summary>
    /// Sets the factor by which the amplitude changes each time the noise is added
    /// </summary>
    [Range(0f, 1f)]
    public float persistence = 0.5f;

    /// <summary>
    /// Resolution of the texture
    /// </summary>
    [Range(2, 512)]
    public int resolution = 256;

    [Range(1, 3)]
    public int dimension = 3;

    /// <summary>
    /// Frequency used for noise generation
    /// </summary>
    [Range(2, 32)]
    public float frequency = 1f;

    /// <summary>
    /// Defines the type of texture to be generated
    /// </summary>
    [Space(10)]
    public TextureType textureType;

    /// <summary>
    /// Base color used when generating the texture
    /// </summary>
    [HideInInspector]
    public Color color = Color.white;

    /// <summary>
    /// Color gradient used when generating the texture
    /// </summary>
    [HideInInspector]
    public Gradient gradient;

    #endregion

    #region Private

    /// <summary>
    /// Generated texture
    /// </summary>
    private Texture2D texture;

    /// <summary>
    /// Generated clamped noise
    /// </summary>
    private float[,] generatedNoise;

    #endregion

    #endregion

    #region Constants

    /// <summary>
    /// Constant string for the title of the Save File Panel
    /// </summary>
    private const string SAVE_TEXTURE = "Save Texture";

    /// <summary>
    /// Constant string for the message displayed in the Save File Name
    /// </summary>
    private const string SAVE_TEXTURE_MESSAGE = "Please enter a file name";

    /// <summary>
    /// Constant string for the available file extensions of the Save File Panel
    /// </summary>
    private const string ALLOWED_TEXTURE_EXTENSIONS = "png";

    /// <summary>
    /// Constant string for the default file name when saving the texture
    /// </summary>
    private const string DEFAULT_TEXTURE_NAME = "Texture";

    #endregion

    #region Events

    /// <summary>
    /// Called when the object is enabled in the scene
    /// </summary>
    private void OnEnable()
    {
        if (texture == null)
        {
            texture = new Texture2D(resolution, resolution, TextureFormat.RGB24, true);
            texture.name = "Procedural Texture";
            texture.filterMode = FilterMode.Trilinear;
            texture.wrapMode = TextureWrapMode.Clamp;
            texture.anisoLevel = 9;
            meshRenderer.material.mainTexture = texture;
        }
        FillNoiseArray();
    }

    /// <summary>
    /// Called every frame
    /// </summary>
    private void Update()
    {
        if (transform.hasChanged)
        {
            transform.hasChanged = false;
            FillNoiseArray();
        }
    }

    #endregion

    #region Public Methods

    /// <summary>
    /// Fiils the noise array
    /// </summary>
    public void FillNoiseArray()
    {
        if (texture.width != resolution)
        {
            texture.Resize(resolution, resolution);
        }
        if (generatedNoise == null)
        {
            generatedNoise = new float[resolution, resolution];
        }
        float min = float.MaxValue;
        float max = float.MinValue;

        Vector3 point00 = transform.TransformPoint(new Vector3(-0.5f, -0.5f));
        Vector3 point10 = transform.TransformPoint(new Vector3(0.5f, -0.5f));
        Vector3 point01 = transform.TransformPoint(new Vector3(-0.5f, 0.5f));
        Vector3 point11 = transform.TransformPoint(new Vector3(0.5f, 0.5f));

        NoiseMethod method = Noise.noiseMethods[(int)noiseType][dimension - 1];
        float stepSize = 1f / resolution;
        for (int y = 0; y < resolution; y++)
        {
            Vector3 point0 = Vector3.Lerp(point00, point01, (y + 0.5f) * stepSize);
            Vector3 point1 = Vector3.Lerp(point10, point11, (y + 0.5f) * stepSize);
            for (int x = 0; x < resolution; x++)
            {
                Vector3 point = Vector3.Lerp(point0, point1, (x + 0.5f) * stepSize);
                point.x += seed;
                point.y += seed;
                point.z += seed;
                float sample = Noise.Sum(method, point, frequency, octaves, lacunarity, persistence);
                if (noiseType != NoiseType.Value)
                {
                    sample = sample * 0.5f + 0.5f;
                }
                generatedNoise[x, y] = sample;
                if (sample < min)
                {
                    min = sample;
                }
                if (sample > max)
                {
                    max = sample;
                }
            }
        }
        ClampGeneratedNoise(min, max);
    }


    public void GenerateTexture()
    {
        if (textureType == TextureType.SquareGrid)
        {
            GenerateSquareGrid();
        }
        else if (textureType == TextureType.HexagonalGrid)
        {
            GenerateHexGrid();
        }
        else
        {
            FillNoiseArray();

            if (terracedHeightMap && textureType == TextureType.HeightMap)
            {
                GenerateTerracedHeightMap();
            }
            else
            {
                for (int x = 0; x < resolution; x++)
                {
                    for (int y = 0; y < resolution; y++)
                    {
                        texture.SetPixel(x, y, textureType == TextureType.Gradient ? gradient.Evaluate(generatedNoise[x, y]) : color * generatedNoise[x, y]);
                    }
                }
                texture.Apply();
            }
        }
    }


    public void ApplyNoiseToTexture()
    {
        Random.InitState(seed);
        float randomValue = Random.Range(1f, 10000f);
        float[,] previousNoise = generatedNoise;
        generatedNoise = new float[resolution, resolution];
        float min = float.MaxValue;
        float max = float.MinValue;

        Vector3 point00 = transform.TransformPoint(new Vector3(-0.5f, -0.5f));
        Vector3 point10 = transform.TransformPoint(new Vector3(0.5f, -0.5f));
        Vector3 point01 = transform.TransformPoint(new Vector3(-0.5f, 0.5f));
        Vector3 point11 = transform.TransformPoint(new Vector3(0.5f, 0.5f));

        NoiseMethod method = Noise.noiseMethods[(int)noiseType][dimension - 1];
        float stepSize = 1f / resolution;
        for (int y = 0; y < resolution; y++)
        {
            Vector3 point0 = Vector3.Lerp(point00, point01, (y + 0.5f) * stepSize);
            Vector3 point1 = Vector3.Lerp(point10, point11, (y + 0.5f) * stepSize);
            for (int x = 0; x < resolution; x++)
            {
                Vector3 point = Vector3.Lerp(point0, point1, (x + 0.5f) * stepSize);
                point.x += randomValue;
                point.y += randomValue;
                point.z += randomValue;
                float sample = Noise.Sum(method, point, frequency, octaves, lacunarity, persistence);
                generatedNoise[x, y] = sample;
                if (sample < min)
                {
                    min = sample;
                }
                if (sample > max)
                {
                    max = sample;
                }
            }
        }
        ClampGeneratedNoise(min, max);
        for (int x = 0; x < resolution; x++)
        {
            for (int y = 0; y < resolution; y++)
            {
                Color color = gradient.Evaluate(previousNoise[x, y] * 0.6f + generatedNoise[x, y] * 0.4f);
                texture.SetPixel(x, y, color);
            }
        }
        texture.Apply();
    }

    /// <summary>
    /// Saves the generated texture to a file
    /// </summary>
    public void SaveTexture()
    {
        if (texture == null)
        {
            return;
        }
        string path = EditorUtility.SaveFilePanelInProject(SAVE_TEXTURE, DEFAULT_TEXTURE_NAME, ALLOWED_TEXTURE_EXTENSIONS, SAVE_TEXTURE_MESSAGE);
        if (!string.IsNullOrEmpty(path))
        {
            byte[] pngData = texture.EncodeToPNG();
            if (pngData != null)
            {
                File.WriteAllBytes(path, pngData);

                // As we are saving to the asset folder, tell Unity to scan for modified or new assets
                AssetDatabase.Refresh();
            }
        }
    }

    #endregion

    #region Private Methods


    private void ClampGeneratedNoise(float min, float max)
    {
        for (int x = 0; x < resolution; x++)
        {
            for (int y = 0; y < resolution; y++)
            {
                float val = generatedNoise[x, y];
                val = (val - min) / (max - min);
                generatedNoise[x, y] = val;
            }
        }
    }


    private void GenerateSquareGrid()
    {
        float gridCellSize = resolution / 10f;
        float lineSize = gridCellSize * 0.1f;

        int verticalStripesGenerated = 0;
        int horizontalStripesGenerated = 0;
        Debug.Log("Grid Cell Size: " + gridCellSize.ToString() + "\nGridCellSize * CellsGenerated: " + (gridCellSize * verticalStripesGenerated).ToString());
        for (int y = 0; y < resolution; y++)
        {
            verticalStripesGenerated = 0;
            for (int x = 0; x < resolution; x++)
            {
                if (x >= (verticalStripesGenerated * gridCellSize) - lineSize && x <= (verticalStripesGenerated * gridCellSize) + lineSize)
                {
                    texture.SetPixel(x, y, Color.red);
                    if (x + 1 >= (verticalStripesGenerated * gridCellSize) + lineSize)
                    {
                        verticalStripesGenerated++;
                    }
                }
                else if (y >= (horizontalStripesGenerated * gridCellSize) - lineSize && y <= (horizontalStripesGenerated * gridCellSize) + lineSize)
                {
                    texture.SetPixel(x, y, Color.red);
                }
                else
                {
                    texture.SetPixel(x, y, Color.green);
                }
                if (y + 1 >= (horizontalStripesGenerated * gridCellSize) + lineSize && x + 1 >= resolution)
                {
                    horizontalStripesGenerated++;
                }
            }
        }
        texture.Apply();
    }

    private void GenerateHexGrid()
    {
        float gridCellSize = resolution / 10f;
        float lineSize = gridCellSize * 0.025f;


        int currentXCell = 1;
        int currentYCell = 1;

        for (int y = 0; y < resolution; y++)
        {
            currentXCell = 1;
            for (int x = 0; x < resolution; x++)
            {
                float xCenter = (currentXCell * gridCellSize - gridCellSize * 0.5f);
                float yCenter = (currentYCell * gridCellSize - gridCellSize * 0.5f);

                float xTopLeft = (xCenter - gridCellSize * 0.5f);
                float yTopLeft = (yCenter + gridCellSize * 0.25f);


                if (IsHexagonBoundary(x, y, gridCellSize * 0.5f, lineSize, currentXCell, currentYCell))
                {
                    texture.SetPixel(x, y, Color.red);
                }
                else
                {
                    texture.SetPixel(x, y, Color.green);
                }
                if (x + 1 > (currentXCell * gridCellSize) + lineSize)
                {
                    float offset = (xCenter - xTopLeft) / ((yCenter + gridCellSize * 0.5f) - yTopLeft);
                    int u = (int)xTopLeft;
                    for (int v = (int)yTopLeft;v<yCenter + gridCellSize * 0.5f; v++)
                    {
                        for (int w = u; w <= u + offset && u + offset < xCenter; w++)
                        {
                            texture.SetPixel(w, v, Color.black);
                        }
                        u += (int)offset;
                    }
                    currentXCell++;
                }

                if (y + 1 > (currentYCell * gridCellSize) + lineSize && x + 1 >= resolution)
                {
                    currentYCell++;
                }
            }
        }
        texture.Apply();
    }


    private bool IsHexagonBoundary(int x, int y, float radius, float lineSize, int currentXCell, int currentYCell)
    {
        float xCenter = currentXCell * radius * 2f - radius;
        float yCenter = currentYCell * radius * 2f - radius;
        
        if ((x <= (xCenter - radius + lineSize) && x >= (xCenter - radius - lineSize)) || (x >= (xCenter + radius - lineSize) && x <= (xCenter + radius + lineSize)))
        {
            if (y >= (yCenter - radius * 0.5f) && y <= (yCenter + radius * 0.5f))
            {
                return true;
            }
        }
        else if ((y <= (yCenter - radius + lineSize) && y >= (yCenter - radius - lineSize)) || (y >= (yCenter + radius - lineSize) && y <= (yCenter + radius + lineSize)))
        {
            if (x >= (xCenter - lineSize) && x <= (xCenter + lineSize))
            {
                return true;
            }
        }

        return false;
    }

    private void GenerateTerracedHeightMap()
    {
        for (int x = 0; x < resolution; x++)
        {
            for (int y = 0; y < resolution; y++)
            {
                texture.SetPixel(x, y, Color.white * HeightMapManager.GetTerraceHeightValue(generatedNoise[x, y], blendHeightMapTerraces, blendFactor));
            }
        }
        texture.Apply();
    }

    #endregion

}
