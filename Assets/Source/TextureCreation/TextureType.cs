﻿using UnityEngine;
using System.Collections;

public enum TextureType
{
    Gradient = 0,
    HeightMap = 1,
    HexagonalGrid = 2,
    SquareGrid = 3,
    TextureCombination = 4
}
