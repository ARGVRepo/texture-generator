﻿using UnityEngine;
using System.Collections;

public static class HeightMapManager
{

    #region Properties

    /// <summary>
    /// Set to true if you want a terraced heightmap
    /// </summary>
    public static bool terraced = false;

    /// <summary>
    /// width of the blending area
    /// </summary>
    public static float terraceBlend = 0.1f;

    #region Private

    /// <summary>
    /// Max value of the DeepWater terrain type
    /// </summary>
    private static float deepWaterLevel = 0.2f;

    /// <summary>
    /// Max value of the DeepWater terrain type
    /// </summary>
    private static float shallowWaterLevel = 0.3f;

    /// <summary>
    /// Max value of the DeepWater terrain type
    /// </summary>
    private static float coastLevel = 0.35f;

    /// <summary>
    /// Max value of the LowestLandHeight terrain type
    /// </summary>
    private static float lowestLandLevel = 0.45f;

    /// <summary>
    /// Max value of the LowLandHeight terrain type
    /// </summary>
    private static float lowLandLevel = 0.70f;

    /// <summary>
    /// Max value of the MediumLandHeight terrain type
    /// </summary>
    private static float mediumLandLevel = 0.85f;

    /// <summary>
    /// Max value of the HighLandHeight terrain type
    /// </summary>
    private static float highLandLevel = 1f;

    #endregion

    #endregion

    #region Public Methods


    public static float GetTerrainHeightValue(TerrainHeight terrain)
    {
        switch (terrain)
        {
            case TerrainHeight.DeepWater:
                return deepWaterLevel;
            case TerrainHeight.ShallowWater:
                return shallowWaterLevel;
            case TerrainHeight.Coast:
                return coastLevel;
            case TerrainHeight.LowLandHeight:
                return lowLandLevel;
            case TerrainHeight.MediumLandHeight:
                return mediumLandLevel;
            case TerrainHeight.HighLandHeight:
                return highLandLevel;
            default:return 0f;
        }
    }


    public static float GetTerraceHeightValue(float value, bool blend, float blendFactor)
    {
        if (blend)
        {
            return GetBlendedTerraceHeightValue(value, blendFactor);
        }
        else
        {
            return GetTerraceHeightValue(value);
        }
    }


    private static float GetBlendedTerraceHeightValue(float value, float blendFactor)
    {
        float factor = terraceBlend * blendFactor;

        if (value <= deepWaterLevel)
        {
            return deepWaterLevel;
        }
        else if (value <= deepWaterLevel + factor)
        {
            float data = (value - deepWaterLevel) / factor;
            if(data < 0.5f)
            {
                Debug.Log("Data: " + data);
            }
            float val = Mathf.Lerp(deepWaterLevel, shallowWaterLevel, (value - deepWaterLevel)/ factor);
            return val;
        }
        else if (value <= shallowWaterLevel)
        {
            return shallowWaterLevel;
        }
        else if (value <= shallowWaterLevel + factor)
        {
            float val = Mathf.Lerp(shallowWaterLevel, coastLevel, (value - shallowWaterLevel) / factor);
            return val;
        }
        else if (value <= coastLevel)
        {
            return coastLevel;
        }
        else if (value <= coastLevel + factor)
        {
            float data = (value - coastLevel) / factor;
            float val = Mathf.Lerp(coastLevel, lowestLandLevel, (value - coastLevel) / factor);
            return val;
        }
        else if (value <= lowestLandLevel)
        {
            return lowestLandLevel;
        }
        else if(value <= lowestLandLevel + factor)
        {
            float val = Mathf.Lerp(lowestLandLevel, lowLandLevel, (value - lowestLandLevel) / factor);
            return val;
        }
        else if (value <= lowLandLevel)
        {
            return lowLandLevel;
        }
        else if(value <= lowLandLevel + factor)
        {
            float val = Mathf.Lerp(lowLandLevel, mediumLandLevel, (value - lowLandLevel) / factor);
            return value;
        }
        else if (value <= mediumLandLevel)
        {
            return mediumLandLevel;
        }
        else if(value <= mediumLandLevel + factor)
        {
            float val = Mathf.Lerp(mediumLandLevel, highLandLevel, (value - mediumLandLevel) / factor);
            return value;
        }
        else
        {
            return highLandLevel;
        }
    }

    private static float GetTerraceHeightValue(float value)
    {
        if(value <= deepWaterLevel)
        {
            return deepWaterLevel;
        }
        else if(value <= shallowWaterLevel)
        {
            return shallowWaterLevel;
        }
        else if(value <= coastLevel)
        {
            return coastLevel;
        }
        else if(value <= lowestLandLevel)
        {
            return lowestLandLevel;
        }
        else if(value <= lowLandLevel)
        {
            return lowLandLevel;
        }
        else if(value <= mediumLandLevel)
        {
            return mediumLandLevel;
        }
        else
        {
            return highLandLevel;
        }

    }

#endregion

}
