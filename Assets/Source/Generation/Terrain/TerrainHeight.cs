﻿public enum TerrainHeight
{
    DeepWater = 0,
    ShallowWater = 1,
    Coast = 2,
    LowLandHeight = 3,
    MediumLandHeight = 4,
    HighLandHeight = 5
}
