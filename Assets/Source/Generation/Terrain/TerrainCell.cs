﻿using UnityEngine;
using System.Collections;

public class TerrainCell
{

    #region Properties

    /// <summary>
    /// X Coordinate of the cell's position on the map
    /// </summary>
    public int XPosition { get; set; }

    /// <summary>
    /// Y Coordinate of the cell's position on the map
    /// </summary>
    public int YPosition { get; set; }

    /// <summary>
    /// Cell elevation
    /// </summary>
    public float HeightValue { get; set; }

    /// <summary>
    /// Type of the terrain based on its elevation
    /// </summary>
    public TerrainHeight Type { get; set; }

    /// <summary>
    /// Feature level of the cell
    /// </summary>
    public float BiomeValue { get; set; }


    public Biome biome
    {
        get { return TerrainManager.GetBiomeFromValue(BiomeValue); }
    }

    /// <summary>
    /// array that contains the neighboring cells
    /// </summary>
    private TerrainCell[] neighbors = new TerrainCell[4];

    #endregion

    #region Public Methods

    /// <summary>
    /// Returns the terraced height value of this cell
    /// </summary>
    /// <returns></returns>
    public float GetTerraceValue()
    {
        float defaultBlend = 0.1f;
        return GetTerraceValue(defaultBlend);
    }

    /// <summary>
    /// Returns the terraced height value of this cell
    /// </summary>
    /// <returns></returns>
    public float GetTerraceValue(float blend)
    {
        return HeightMapManager.GetTerraceHeightValue(HeightValue, TerrainManager.blendTerraces, blend);
    }

    /// <summary>
    /// Sets a neighbor
    /// </summary>
    /// <param name="direction"></param>
    /// <param name="neighbor"></param>
    public void SetNeighbor(Direction direction, TerrainCell neighbor)
    {
        if (neighbor != null)
        {
            neighbors[(int)direction] = neighbor;
        }
    }

    /// <summary>
    /// Gets a neighbor
    /// </summary>
    /// <param name="direction"></param>
    /// <returns></returns>
    public TerrainCell GetNeighbor(Direction direction)
    {
        return neighbors[(int)direction];
    }

#endregion

}
