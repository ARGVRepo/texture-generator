﻿using UnityEngine;
using System.Collections;

public static class TerrainManager
{

    #region Properties


    public static TerrainCell[,] terrainCells;

    /// <summary>
    /// Array that contains the height values for each pixel
    /// </summary>
    public static float[,] heightData;
    
    /// <summary>
    /// Array that contains the height values for each pixel
    /// </summary>
    public static float[,] biomeData;

    /// <summary>
    /// Set to true to blend the terraces when generating a terraced map
    /// </summary>
    public static bool blendTerraces;

    #endregion

    #region Public Methods


    public static Biome GetBiomeFromValue(float value)
    {
        return Biome.Desert;
    }

    /// <summary>
    /// Initializes the Terrain Cells array
    /// </summary>
    /// <param name="width"></param>
    /// <param name="length"></param>
    public static void InitCells(int width, int length)
    {
        terrainCells = new TerrainCell[width, length];
    }

#endregion

}
