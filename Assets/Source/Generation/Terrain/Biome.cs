﻿public enum Biome
{
    Forest = 0,
    Hills = 1,
    Plains = 2,
    Desert = 3,
    MountainRange = 4
}
