﻿public enum NoiseType
{
    Value = 0,
    Perlin = 1,
    BillowedPerlin = 2,
    RidgedPerlinNoise = 3,
    IQNoise = 4

}
