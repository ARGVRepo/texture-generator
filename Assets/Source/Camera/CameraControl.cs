﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraControl : MonoBehaviour
{

    #region Properties

    /// <summary>
    /// Reference to the texture creator in the scene
    /// </summary>
    public TextureCreator textureObject;

    /// <summary>
    /// Velocity of the camera when zoomin in/out
    /// </summary>
    public int zoomSpeed = 100;

    /// <summary>
    /// Max distance the camera can move when zooming in
    /// </summary>
    public float maxZoomDistance = 0.5f;

    /// <summary>
    /// Max distance the camera can move when zooming out
    /// </summary>
    public float minZoomDistance = 1f;

    /// <summary>
    /// Maximum speed of the camera when moving
    /// </summary>
    public float maxMoveSpeed = 15f;

    /// <summary>
    /// Minimum speed of the camera when moving
    /// </summary>
    public float minMoveSpeed = 1f;

    /// <summary>
    /// distance to target when play mode is started
    /// </summary>
    public float standardDistanceToTarget = 1f;

    #region Private

    /// <summary>
    /// flag to let us know when the initial focus movement has finished
    /// </summary>
    private bool initialFocusFinished = false;

    /// <summary>
    /// Position of the texture creator. Assigned in Start()
    /// </summary>
    private Vector3 targetPosition;

    /// <summary>
    /// Keeps that of the previous position of the camera
    /// </summary>
    private Vector3 initialPosition;

    /// <summary>
    /// position of the camera after the initial focus
    /// </summary>
    private Vector3 positionAfterFocus;

    /// <summary>
    /// returns false if its position has been moved and needs to be reseted
    /// </summary>
    private bool fixedPos;

    #endregion

    #endregion

    #region Input Keys

    /// <summary>
    /// Constant key to move the camera forward
    /// </summary>
    public const KeyCode UP = KeyCode.W;

    /// <summary>
    /// Constant key to move the camera back
    /// </summary>
    public const KeyCode DOWN = KeyCode.S;

    /// <summary>
    /// Constant key to move the camera to the left
    /// </summary>
    public const KeyCode LEFT = KeyCode.A;

    /// <summary>
    /// Constant key to move the camera to the right
    /// </summary>
    public const KeyCode RIGHT = KeyCode.D;

    /// <summary>
    /// Constant key to zoom in the camera
    /// </summary>
    public const KeyCode ZOOM_IN = KeyCode.R;

    /// <summary>
    /// Constant key to zoom out the camera
    /// </summary>
    public const KeyCode ZOOM_OUT = KeyCode.F;

    #endregion

    #region Events

    /// <summary>
    /// Called the frame after the object is created
    /// </summary>
    void Start()
    {
        targetPosition = textureObject.transform.position;
        targetPosition.z -= standardDistanceToTarget;

        initialPosition = transform.position;
    }

    /// <summary>
    /// Called every frame this gameobject is enabled
    /// </summary>
    void Update()
    {
        if (initialFocusFinished)
        {
            HandleInput();
        }
        else
        {
            FocusOnTextureCreator();
        }
    }

    #endregion

    #region Private Methods

    /// <summary>
    /// Focus the camera on the texture creator
    /// </summary>
    private void FocusOnTextureCreator()
    {
        Vector3 currentPosition = transform.position;
        Vector3 distanceToTarget = targetPosition - currentPosition;
        Vector3 distanceToInitialPos = currentPosition - initialPosition;
        Vector3 direction = distanceToTarget.normalized;

        float floatToTarget = Mathf.Max(Mathf.Abs(distanceToTarget.x), Mathf.Abs(distanceToTarget.y), Mathf.Abs(distanceToTarget.z));
        float floatToInitialPos = Mathf.Max(distanceToInitialPos.x, distanceToInitialPos.z);
        float velocity = maxMoveSpeed;

        if (floatToTarget >= standardDistanceToTarget)
        {
            if (floatToTarget <= standardDistanceToTarget * 2f)
            {
                float step = (floatToTarget - standardDistanceToTarget) / (standardDistanceToTarget * 2f);

                velocity = Mathf.SmoothStep(minMoveSpeed, maxMoveSpeed, step);
            }
            else if (floatToInitialPos <= standardDistanceToTarget * 2f)
            {
                float step = floatToInitialPos / (standardDistanceToTarget * 2f);

                velocity = Mathf.SmoothStep(minMoveSpeed, maxMoveSpeed, step);
            }
            velocity *= Time.deltaTime;
            transform.position += direction * velocity;
        }
        else
        {
            initialFocusFinished = true;
            positionAfterFocus = transform.position;
        }
    }

    /// <summary>
    /// Handles user input to control the camera
    /// </summary>
    private void HandleInput()
    {
        Vector3 forward = transform.forward;
        float velocity = zoomSpeed * Time.deltaTime;
        float distance = Vector3.Distance(transform.position, targetPosition);
        float absDistance = Mathf.Abs(distance);
        if (Input.GetKey(ZOOM_IN))
        {
            fixedPos = false;
            if (absDistance > standardDistanceToTarget * 0.2f)
            {
                transform.position += forward * velocity;
            }
        }
        else if (Input.GetKey(ZOOM_OUT))
        {
            fixedPos = false;
            if (absDistance < standardDistanceToTarget * 1.8f)
            {
                transform.position -= forward * velocity;
            }
        }
        else if (!fixedPos)
        {
            if (Mathf.Abs(absDistance - standardDistanceToTarget) < velocity)
            {
                transform.position = positionAfterFocus;
                fixedPos = true;
            }
            else
            {
                if(absDistance - standardDistanceToTarget > 0)
                {
                    transform.position += forward * velocity;
                }
                else
                {

                    transform.position -= forward * velocity;
                }
            }
        }
    }

    #endregion

}
